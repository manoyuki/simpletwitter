package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//message削除
	public void delete(String messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = Integer.parseInt(messageId);
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//message編集
	public Message edit(String messageId) {
		Connection connection = null;

		try {
			Integer id = Integer.parseInt(messageId);
			connection = getConnection();
			Message message = new MessageDao().edit(connection, id);
			commit(connection);
			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {
		Connection connection = null;
		connection = getConnection();
		try {
			new MessageDao().update(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;
		String startDate;
		String endDate;

		//絞り込み機能
		if (StringUtils.isEmpty(start)) {
			startDate = "2020/01/01 00:00:00";
		} else {
			startDate = start + " 00:00:00";
		}

		if (StringUtils.isEmpty(end)) {
			endDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		} else {
			endDate = end + " 23:59:59";
		}

		Connection connection = null;
		try {
			connection = getConnection();
			//idがnullではないとき、idにuserIdをセット
			Integer id = null;
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);

			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}